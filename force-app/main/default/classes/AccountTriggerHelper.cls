/**
*@Name Utilities
*@Description Collection of generic utilities used across different classes.
*@Author Daniel Llewellyn (Kenji776@gmail.com)
*@version	2020-03-19	Daniel Llewellyn
**/

public with sharing class AccountTriggerHelper {
    
    //names of default opportunities to create
    public static set<string> defaultOpportunityNames = new set<string>{'Default Residential Opp','Default Commercial Opp'};
    
    //name of email template to use to send notification email when missing opportunities are detected
    public static string missingOpportunityEmailTemplateName = 'Missing_Opportunities_On_Account';
    
    //list of stage names an account can have to validate the default opportunities exist.
    public static list<string> validTypesToMatch = new list<string>{'Prospect'};
    
    /**
    *@Description handler for after insert events. Dispatches incoming accounts to other methods for processing.
    *@Param accounts list of incoming accounts
    *@Return list of contacts modified by called methods ready to be inserted into database.
    **/
    public list<Account> handleAfterInsert(list<Account> accounts)
    {
        //Create opportunities with the given names, allow the method to set whatever defaults it wants for the closedate and stagename, do insert.
        list<Opportunity> createdOpportunites = utilities.createOpportunities(accounts, defaultOpportunityNames, null, null, true);

        return accounts;
    }    

    /**
    *@Description handler for before update events. Dispatches incoming accounts to other methods for processing.
    *@Param accounts list of incoming accounts
    *@Return list of accounts modified by called methods ready to be inserted into database.
    **/
    public list<Account> handleBeforeUpdate(list<Account> accounts, map<Id,Account> oldRecords)
    {
        //Create opportunities with the given names, allow the method to set whatever defaults it wants for the closedate and stagename, do insert.
        map<Account,set<string>> accountsWithMissingOpps = checkForDefaultOpportunitiesOnAccount(accounts);

        return accounts;
    }    

    public map<Account,set<string>> checkForDefaultOpportunitiesOnAccount(list<Account> accounts)
    {
        //map of accounts to a set of missing opportunity names.
        map<Account,set<string>> accountsWithMissingOpps = new map<Account,set<string>>();

        //use a query with a subquery to get the account and related opporunity data in one query
        Map<Id,Account> acctsWithOpps = new Map<Id,Account>([SELECT Id, type, ownerid, name, (SELECT Id, name FROM Opportunities) FROM Account where id in :accounts]);

        map<Id,Account> existingAccountsValues = new map<Id,Account>(accounts);
        
        //iterate over every account so we can check if its child opportunities exist. If not create them and queue and email to be sent.
        for(account thisAccount : acctsWithOpps.values())
        {
            //check to see if this accounts type name is in the list of valid type that need to have their opportunities checked (could also filter this in the query)
            if(validTypesToMatch.contains(existingAccountsValues.get(thisAccount.Id).type))
            {
                //keep track of all the opportunities that are missing for this account
                set<string> missingOpportunityNames = new set<string>();

                //loop over every default opportunity name that should exist
                for(string defaultOpportunityName :  defaultOpportunityNames)
                {
                    boolean matchFound = false;
                    //loop over every opportunity in this account and look for a match
                    for(Opportunity thisOpp : thisAccount.Opportunities)
                    {
                        //if match is found, mark it as such and break the loop
                        if(thisOpp.name.equals(defaultOpportunityName))
                        {
                            matchFound = true;
                            break;
                        }
                    }

                    //if no match is found after looking at every combo then add this account to our list.
                    if(!matchFound)
                    {
                        missingOpportunityNames.add(defaultOpportunityName);
                    }
                }

                //if the set of missing opportunity names is not empty for this account then add it to our map
                if(!missingOpportunityNames.isEmpty()) accountsWithMissingOpps.put(thisAccount,missingOpportunityNames);
            }
        }

        //create the missing opportunities
        list<Opportunity> createdOpportunites = utilities.createOpportunities(accountsWithMissingOpps, null, null, true);

        //get a set of all the owner ids so we can send emails to them.
        map<id,account> userIds = new map<id,account>();

        for(Account thisAccount : accountsWithMissingOpps.keySet())
        {
            userIds.put(thisAccount.ownerId,thisAccount);
        }

        //send emails to users that default opportunities were missing
        list<Messaging.SendEmailResult> messageResult = utilities.sendEmailTemplateToUsers(userIds, missingOpportunityEmailTemplateName);
        return accountsWithMissingOpps;
    }
}