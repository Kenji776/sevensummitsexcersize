public with sharing class AccountTriggerHandler {
	/**
    *@Description handles all incoming trigger operations and dispatches as required
    *@Param workingRecords new or updated incoming records
    *@Param oldRecords previous versions of records if available. Not all working records will neccessarily have a matching oldRecords entry
    *@Param triggerEvent the type of operation occuring
    *@Return void
    **/
    public static void dispatch(List<Account> workingRecords, Map<Id,Account> oldRecords, System.TriggerOperation triggerEvent )
    {
        switch on triggerEvent {
            when AFTER_INSERT {
                onAfterInsert(workingRecords);
            }
            when BEFORE_UPDATE {
                onBeforeUpdate(workingRecords, oldRecords);
            }       
        }
    }

    /**
    *@Description dispatches after insert events to the helper.
    *@Param accounts new or updated incoming records
    *@Return void
    **/
	public static void onAfterInsert(list<Account> accounts)
	{
		AccountTriggerHelper ATH = new AccountTriggerHelper();
		accounts = ATH.handleAfterInsert(accounts);
    }

    /**
    *@Description dispatches before update events to the helper. 
    *@Param accounts new or updated incoming records
    *@Return void
    **/
	public static void onBeforeUpdate(list<Account> accounts, map<Id,Account> oldRecords)
	{
		AccountTriggerHelper ATH = new AccountTriggerHelper();
		accounts = ATH.handleBeforeUpdate(accounts,oldRecords);
	}
}