/**
*@Name Utilities
*@Description Collection of generic utilities used across different classes.
*@Author Daniel Llewellyn (Kenji776@gmail.com)
*@version	2020-03-19	Daniel Llewellyn
**/
public with sharing class Utilities 
{

    /**
    *@Description Creates a list of opportunities with the given data.
    *@param oppName the name of the opportunity
    *@param closeDate the close date of the opportunity 
    *@param stageName the stage name of the opportunity
    *@param doInsert should the created opportunity be inserted or just returned
    *@return the created opportunities 
    **/
    public static list<Opportunity> createOpportunities(list<Account> parentAccounts, set<string> oppNames, date closeDate, string stageName, boolean doInsert)
    {
        map<Account,set<string>> accountMap = new map<Account,set<string>>();
        for(Account thisAccount : parentAccounts)
        {
            accountMap.put(thisAccount,oppNames);
        }
        return createOpportunities(accountMap, closeDate, stageName, doInsert);
    }

    /**
    *@Description Creates a list of opportunities with the given data.
    *@param oppNames a list of opportunity names
    *@param closeDate the close date of the opportunity 
    *@param stageName the stage name of the opportunity
    *@param doInsert should the created opportunity be inserted or just returned
    *@return the created opportunities 
    **/
    public static list<Opportunity> createOpportunities(Map<Account,set<string>> accountMap, date closeDate, string stageName, boolean doInsert)
    {
        list<Opportunity> opps = new list<Opportunity>();

        //set defaults in case they are null. May want to store these defaults in a custom setting or metadata if they should be configurable.
        closeDate = closeDate != null ? closeDate : date.today();
        stageName = stageName != null ? stageName : 'Prospecting';
        
        for(Account thisAccount :  accountMap.keySet())
        {
            set<string> oppNames = accountMap.get(thisAccount);
            for(string thisOppName : oppNames)
            {
                Opportunity thisOpp = new Opportunity();
                thisOpp.accountId = thisAccount.Id;
                thisOpp.name = thisOppName;
                thisOpp.closeDate = closeDate;
                thisOpp.stageName = stageName;
                opps.add(thisOpp);
            }
        }
        if(doInsert) database.insert(opps,true);

        return opps;
    }

        /**
    *@Description Sends a given email template to all users in the map with the related data defined in the map
    *@param userIdToRelatedObjectMap map of user ids to related object data used to populate the email template. Additional data will not work if sending to a user record.
    *@param templateName the developer name of the email template to use
    *@return the sent emails 
    **/
    public static list<Messaging.SendEmailResult> sendEmailTemplateToUsers(map<id,sObject> recipIdToRelatedObjectMap, string templateName)
    {
        //this will error if the given email template does not exist.
        list<EmailTemplate> emailTemplate =[Select id from EmailTemplate where developername = :templateName limit 1];
        if(emailTemplate.isEmpty())
        {
            throw new applicationException('No Email template named ' + templateName + ' could be found. Email will not be sent. ');
        }

        Map<Id,User> userEmails = new map<Id,User>([select email from user where id in :recipIdToRelatedObjectMap.keySet()]);

        List<Messaging.SingleEmailMessage > messagesToSend = new List<Messaging.SingleEmailMessage>();

        for (Id recipId : recipIdToRelatedObjectMap.keySet()) 
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(recipId);
            mail.setTreatTargetObjectAsRecipient(true);
            mail.setTemplateId(emailTemplate[0].Id);
            mail.setSaveAsActivity(false);

            //because salesforce has crazy restrictions around email sending, emails destined for a user record cannnot contain additional data for the email template.
            if(recipId.getSObjectType().getDescribe().getName() != 'User') mail.setWhatId(recipIdToRelatedObjectMap.get(recipId).Id);
            messagesToSend.add(mail);
        }

        //cannot send emails in test context.
        if(!Test.isRunningTest()) 
        {
            return  Messaging.sendEmail(messagesToSend);
        }
        return null;
    }

    public class applicationException extends Exception {}
}