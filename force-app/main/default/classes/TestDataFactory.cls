/**
*@Name TestDataFactory
*@Description Creates test records for unit testing.
*@Author Daniel Llewellyn (Kenji776@gmail.com)
*@version	2020-03-19	Daniel Llewellyn
**/
public with sharing class TestDataFactory {

    public static list<Account> createTestAccounts(integer numberOfAccounts, boolean doInsert)
    {
        list<Account> accountsToCreate = new list<Account>();
        for(integer i=0; i<numberOfAccounts; i++)
        {
            Account thisAccount = new Account();
            thisAccount.name = 'New Test Account ' + i;
            accountsToCreate.add(thisAccount);
        }
        if(doInsert) database.insert(accountsToCreate,true);
        return accountsToCreate;
    }

}