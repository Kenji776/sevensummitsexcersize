/**
*@Name AccountTrigger
*@Description all trigger logic related to accounts.
*@Author Daniel Llewellyn (Kenji776@gmail.com)
*@version	2020-03-19	Daniel Llewellyn
**/
trigger AccountTrigger on Account (after insert, before update) 
{
    AccountTriggerHandler.dispatch(Trigger.new, Trigger.oldMap, Trigger.operationType);
}