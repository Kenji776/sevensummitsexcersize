/**
*@Name AccountTriggersTest
*@Description Test classes for all methods invoked by account trigger processses
*@Author Daniel Llewellyn (Kenji776@gmail.com)
*@version	2020-03-19	Daniel Llewellyn
**/
@isTest
public with sharing class AccountTriggersTest 
{
    /**
    *@Description tests logic related to the creation of default opportunities in the account create trigger
    **/
    @isTest
    public static void testAccountCreateCreateOpportunities()
    {
        Test.StartTest();
            //when an account is created it should have two default opportunities created for it.
            Account testAccount = TestDataFactory.createTestAccounts(1, true)[0];

        Test.StopTest();

        Map<Id,Account> acctsWithOpps = new Map<Id,Account>([SELECT Id, type, ownerid, name, (SELECT Id, name FROM Opportunities) FROM Account where id = :testAccount.Id]);

        testAccount = acctsWithOpps.get(testAccount.Id);

        //test that a number of opportunities the same size as the list of opportunity names was created.
        system.assertEquals(AccountTriggerHelper.defaultOpportunityNames.size(), testAccount.Opportunities.size(),  'The correct number of opportunities ('+AccountTriggerHelper.defaultOpportunityNames.size()+') were not created');

    }
    /**
    *@Description tests logic related to the creation of default opportunities in the account update trigger
    **/
    @isTest
    public static void testAccountUpdateCreateOpportunities()
    {
            //when an account is created it should have two default opportunities created for it.
            Account testAccount = TestDataFactory.createTestAccounts(1, true)[0];

            //lets delete the opportunities from it then update the account.
            database.delete([select id from Opportunity where accountId = :testAccount.Id]);

            //make sure the proper type is set on the account to trigger the checking logic
            testAccount.type = AccountTriggerHelper.validTypesToMatch[0];

            update testAccount;

            //now the opportunities should have been created again
            list<Opportunity> opps = [select id from Opportunity where accountId = :testAccount.Id];

            //check to make sure they exist
            system.assertEquals(AccountTriggerHelper.defaultOpportunityNames.size(), opps.size(), 'The correct number of opportunities ('+AccountTriggerHelper.defaultOpportunityNames.size()+') were not created');
    
            //delete them again and change the account status to something besides prospecting. they should not get recreated
            delete opps;

            testAccount.type = null;
            
            update testAccount;

            //ooportunities should not exist
            opps = [select id from Opportunity where accountId = :testAccount.Id];
            system.assertEquals(0,opps.size(), 'Opportunities were created when they should not have been.');
    }
}